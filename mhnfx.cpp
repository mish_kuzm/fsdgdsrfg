﻿#include <iostream>


class Vector
{
public:
	Vector() : x(0), y(0), z(5)
	{}
	Vector(double _x,double _y,double _z) : x(_x),y(_y),z(_z)
	{}
	void Show()
	{
		std::cout << x << ' ' << y << ' ' << z;
	}
	void Show_Z()
	{
		std::cout << z;
	}

private:
		double x;
		double y;
		double z;
};

int main()
{
	Vector w;
	w.Show_Z();
}